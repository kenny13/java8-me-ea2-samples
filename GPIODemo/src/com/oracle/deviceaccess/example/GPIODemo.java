/*
 * Copyright (c) 2011, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms. 
 */
package com.oracle.deviceaccess.example;

import java.io.IOException;


import javax.microedition.midlet.MIDlet;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.gpio.GPIOPin;
import com.oracle.deviceaccess.gpio.GPIOPinConfig;
import com.oracle.deviceaccess.gpio.GPIOPort;
import com.oracle.deviceaccess.gpio.PinEvent;
import com.oracle.deviceaccess.gpio.PinListener;

/**
 * Demostrates basic GPIO functionality.
 * 
 * <p><b>Target Platforms</b>: emulator and KEIL MCBSTM32F200 development board.</p>
 */ 
public class GPIODemo extends MIDlet {

    static final String LED5_PIN_NAME = "LED 5";
    
    static final String LED6_PIN_NAME = "LED 6";
    
    static final int LED_PORT_ID = 8;
    static final String LED_PORT_NAME = "LEDS";
    
    boolean bFirst = false;
    boolean loopFlag = true;
    
    private GPIOPin led5 = null;
    private GPIOPort ledPort = null;
    private GPIOPin button2 = null;
    private GPIOPin button3 = null;
    
    public void startApp() {
        if(bFirst == false) {

            System.out.println("Starting GPIO Demo");
            try {
            	led5 = (GPIOPin)PeripheralManager.open(new GPIOPinConfig(
                		6, 6, GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                		GPIOPinConfig.TRIGGER_NONE, false));
                button2 = (GPIOPin)PeripheralManager.open(new GPIOPinConfig(
                		2, 13, GPIOPinConfig.DIR_INPUT_ONLY, GPIOPinConfig.MODE_INPUT_PULL_UP,
                		GPIOPinConfig.TRIGGER_BOTH_EDGES, true));
                button3 = (GPIOPin)PeripheralManager.open(new GPIOPinConfig(
                		6, 15, GPIOPinConfig.DIR_INPUT_ONLY, GPIOPinConfig.MODE_INPUT_PULL_UP,
                		GPIOPinConfig.TRIGGER_BOTH_EDGES, true));
                ledPort = (GPIOPort)PeripheralManager.open(LED_PORT_ID);
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("Open pin or port fail");
                return;
            }
            
            System.out.println("set listener for button 2,3");
            try {
                button2.setInputListener(button2Listener);
                button3.setInputListener(button3Listener);
            } catch (Exception ex) {
                ex.printStackTrace();
            } 

            bFirst = true;
        } else {
            System.out.println("GPIO Demo is already started...");
        }
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
        bFirst = false;
        if(led5 != null) {
            try {
                led5.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            led5 = null;
        }
        if(ledPort != null){
            try {
                ledPort.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            ledPort = null;
        }
        if(button2 != null){
            try {
                button2.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            button2 = null;
        }
        if(button3 != null){
            try {
                button3.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            button3 = null;
        }
    }
    
    PinListener button2Listener = new PinListener() {
        public void valueChanged(PinEvent event) {
            GPIOPin pin = (GPIOPin)event.getPeripheral();
            System.out.println("listener2");
            try {
                System.out.println("value:  "+ pin.getValue());
                led5.setValue(!pin.getValue());
            } catch (Exception ex) {
                ex.printStackTrace();
            } 
        }
    };    
    
    PinListener button3Listener = new PinListener() {
        public void valueChanged(PinEvent event) {
            GPIOPin pin = (GPIOPin)event.getPeripheral();
            System.out.println("listener3");
            try {
                System.out.println("value:  "+ pin.getValue());
                if(!pin.getValue()) {
                    ledPort.setValue(3 - (led5.getValue() ? 1 : 0));
                }else{
                    ledPort.setValue(0);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } 
        }
    };
}
