/*
 * Copyright (c) 2012, 2013, Oracle and/or its affiliates. All rights reserved.
 */
package example.i2c;

import com.oracle.deviceaccess.*;
import com.oracle.deviceaccess.i2cbus.I2CDevice;
import com.oracle.deviceaccess.i2cbus.I2CDeviceConfig;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.microedition.midlet.MIDlet;

/**
 * Demostrates basic I2C functionality.
 * 
 * <p><b>Target Platforms</b>: emulator.</p>
 */ 
public class I2CDemo extends MIDlet implements Runnable {

    public void startApp() {
        Thread t = new Thread(this);
        t.start();
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public void run() {
        I2CDevice slave = null;
        final int slaveAddress = 68;
        final int bufferLength = 256;
        ByteBuffer txBuf = ByteBuffer.wrap(new byte[bufferLength]);
        ByteBuffer rxBuf = ByteBuffer.wrap(new byte[bufferLength]);
        int i;

        for (i = 0; i < bufferLength; i++) {
            txBuf.put(i, (byte) i);    //fill in txBuf
        }

        try {
            writeLog("Trying to acquire I2C slave by address \'" + slaveAddress + "\'");
            I2CDeviceConfig config = new I2CDeviceConfig(PeripheralConfig.DEFAULT,
                slaveAddress,
                PeripheralConfig.DEFAULT,
                PeripheralConfig.DEFAULT);
            slave = (I2CDevice) PeripheralManager.open(I2CDevice.class, config);

            writeLog("Writing to I2C slave");
            slave.write(txBuf);

            writeLog("Reading from I2C slave");
            slave.read(rxBuf);

            writeLog("Comparing TX and RX buffers...");
            for (i = 0; i < bufferLength; i++) {
                if (txBuf.get(i) != rxBuf.get(i)) {
                    writeLog("Mismatch of TX/RX data at index " + i + ".");
                    break;
                }
            }
            if (i >= bufferLength) {
                writeLog("TX/RX data match.");
            }

        } catch (PeripheralTypeNotSupportedException ex) {
            writeLog("I2C Not supported.");
            return;
        } catch (PeripheralNotFoundException ex) {
            writeLog("I2C slave does not exist.");
            return;
        } catch (UnavailablePeripheralException ex) {
            writeLog("I2C slave not accessible.");
            return;
        } catch (SecurityException ex) {
            writeLog("Not permitted to access I2C slave.");
            return;
        } catch (IOException ex) {
            writeLog("IO Exception while trying to access I2C slave.");
            return;
        } finally {
            if (slave != null) {
                try {
                    slave.close();
                } catch (IOException ex) {
                    writeLog("IO Exception while trying to close I2C slave.");
                }
            }
        }
        destroyApp(true);
        notifyDestroyed();

    }

    private void writeLog(String log) {
        System.out.println("[I2CDemo] " + log);
    }
}
