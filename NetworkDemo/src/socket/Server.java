/*
 *
 * Copyright (c) 2009, 2012, Oracle and/or its affiliates. All rights reserved.

 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Oracle Corporation nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package socket;

import javax.microedition.io.Connector;
import javax.microedition.io.ServerSocketConnection;
import javax.microedition.io.SocketConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Server implements Runnable {
    private SocketMIDlet parent;

    private boolean stop;

    InputStream is;

    OutputStream os;

    SocketConnection sc;

    ServerSocketConnection scn;

    Sender sender;

    private int port;

    public Server (SocketMIDlet m, int p) {
        parent = m;
        port = p;
    }

    public void start () {
        Thread t = new Thread (this);
        t.start ();
    }

    public void run () {
        String portString = String.valueOf (port);
        try {
            System.out.println ("Waiting for connection on port " + portString);
            scn = (ServerSocketConnection) Connector.open (
                "socket://:"
                    + portString);

            // Wait for a connection.
            sc = (SocketConnection) scn.acceptAndOpen ();
            System.out.println ("Connection accepted");
            is = sc.openInputStream ();
            os = sc.openOutputStream ();
            sender = new Sender (os);

            // Sending server messages
            sender.send ("Server String");

            while (true) {
                StringBuffer sb = new StringBuffer ();
                int c;

                while (((c = is.read ()) != '\n') && (c != -1)) {
                    sb.append ((char) c);
                }

                if (c == -1) {
                    break;
                }

                System.out.println ("Message received - " + sb.toString ());
            }

            stop ();
            System.out.println ("Connection is closed");
            parent.notifyDestroyed ();
            parent.destroyApp (true);
        }
        catch (IOException ioe) {
            if (ioe.getMessage ().indexOf ("serversocket") >= 0 &&
                ioe.getMessage ().indexOf ("open") >= 0) {
                System.out.println ("Server Port " + portString + " is already taken.");
            }
            else {
                if (!stop) {
                    ioe.printStackTrace ();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace ();
        }
    }

    /**
     * Close all open streams
     */
    public void stop () {
        try {
            stop = true;

            if (is != null) {
                is.close ();
            }

            if (os != null) {
                os.close ();
            }

            if (sc != null) {
                sc.close ();
            }

            if (scn != null) {
                scn.close ();
            }
        }
        catch (IOException ioe) {
        }
    }
}
