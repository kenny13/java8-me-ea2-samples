/*
 *
 * Copyright (c) 2009, 2012, Oracle and/or its affiliates. All rights reserved.

 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Oracle Corporation nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package socket;

import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.SocketConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Client implements Runnable {
    private SocketMIDlet parent;

    private boolean stop;

    InputStream is;

    OutputStream os;

    SocketConnection sc;

    Sender sender;

    private String address;
    
    private int port;

    public Client (SocketMIDlet m, String a, int p) {
        parent = m;
        address = a;
        port = p;
    }

    /**
     * Start the client thread
     */
    public void start () {
        Thread t = new Thread (this);
        t.start ();
    }

    public void run () {
        String portString = String.valueOf (port);
        try {
            sc = (SocketConnection) Connector.open (
                "socket://"+ address + ":"
                    + portString);
            System.out.println ("Connected to server " + address + " on port "
                    + portString);
            is = sc.openInputStream ();
            os = sc.openOutputStream ();

            // Start the thread for sending messages - see Sender's main
            // comment for explanation
            sender = new Sender (os);

            sender.send ("Client messages");
            
            // Loop forever, receiving data
            while (true) {
                StringBuffer sb = new StringBuffer ();
                int c;

                while (((c = is.read ()) != '\n') && (c != -1)) {
                    sb.append ((char) c);
                }

                if (c == -1) {
                    break;
                }

                // Display message to user
                System.out.println ("Message received - " + sb.toString ());
            }

            stop ();
            System.out.println ("Connection closed");
            parent.notifyDestroyed ();
            parent.destroyApp (true);
        }
        catch (ConnectionNotFoundException cnfe) {
            System.out.println ("Client Please run Server MIDlet first on server "
                    + address + " port " + portString);
        }
        catch (IOException ioe) {
            if (!stop) {
                ioe.printStackTrace ();
            }
        }
        catch (Exception e) {
            e.printStackTrace ();
        }
    }

    /**
     * Close all open streams
     */
    public void stop () {
        try {
            stop = true;

            if (sender != null) {
                sender.stop ();
            }

            if (is != null) {
                is.close ();
            }

            if (os != null) {
                os.close ();
            }

            if (sc != null) {
                sc.close ();
            }
        }
        catch (IOException ioe) {
        }
    }
}
