/*
 *
 * Copyright (c) 2009, 2013, Oracle and/or its affiliates. All rights reserved.

 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Oracle Corporation nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package socket;

import javax.microedition.midlet.MIDlet;

/**
 * In this application one device acts as the socket server, and the other as the socket client.<br/>
 * 
 * Socket server accepts incoming client connection, sends a message to the connected client and 
 * prints the response message received from it.<br/> 
 * 
 * Socket client connects to the server, sends a message to the connected server and 
 * prints the response message received from it.
     
 * <p>Following user properties in the application descriptor should be adjusted to run this application 
 * either as socket server or as socket client:
 * 
 * <ul>     
 *    <li><b>Oracle-Demo-Network-Mode:</b> <i>[Server|Client]</i> - the mode to run the application</li> 
 *    <li><b>Oracle-Demo-Network-Address:</b> <i>(server address)</i> - target address to connect (used by socket client only)</li> 
 *    <li><b>Oracle-Demo-Network-Port:</b> <i>(port number)</i> - port number</li>
 * </ul></p>  
 *  
 * <p><b>Target Platforms</b>: emulator, CLDC/MEEP enabled boards.</p>
 */
public class SocketMIDlet extends MIDlet {
    private static final int DEFAULT_PORT = 5000;

    private static final String DEFAULT_ADDRESS = "localhost";

    private static final String SERVER = "Server";

    private static final String CLIENT = "Client";

    private boolean isPaused;

    private Server server;

    private Client client;

    public SocketMIDlet () {

        String mode = getAppProperty("Oracle-Demo-Network-Mode");
        if(mode == null) {
            mode = SERVER;
        }
        
        String address = getAppProperty("Oracle-Demo-Network-Address");
        if(address == null) {
            address = DEFAULT_ADDRESS;
        }
        
        int port; 
        
        try {
            port = Integer.parseInt(getAppProperty("Oracle-Demo-Network-Port"));
        } catch (NumberFormatException nfe) {
            port = DEFAULT_PORT;
        }

        if (mode.equals (SERVER)) {
            server = new Server (this, port);
            server.start ();
        } else if (mode.equals (CLIENT)) {
            client = new Client (this, address, port);
            client.start ();
        } else {
            notifyDestroyed ();
        }
    }

    public boolean isPaused () {
        return isPaused;
    }

    public void startApp () {
        isPaused = false;
    }

    public void pauseApp () {
        isPaused = true;
    }

    public void destroyApp (boolean unconditional) {
        if (server != null) {
            server.stop ();
        }

        if (client != null) {
            client.stop ();
        }
    }
}
