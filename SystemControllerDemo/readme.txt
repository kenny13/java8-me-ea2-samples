#
# Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
#

System Controller Sample
------------------------

Overview:
---------

This sample demonstrates the following functionality:
- MVM
- IMlet auto-start
- AMS API
- Logging API
- DAAPI v.C GPIO (LEDs and Buttons)
- DAAPI v.C watchdog

System IMlet: SystemControllerIMlet.
  It controls lifecycle of Application IMlets.
  NOTE: It must be signed if not emulated device is used!

Application IMlet: any other IMlet.
  The sample includes three "stubbed" Application IMlets which do nothing.
  They may be substituted by any real Application IMlets which work on the target platform.
  NOTE: System IMlet uses three Buttons and three LEDs (see below).
        Be sure this does not conflict with a behavior of real Application IMlet(s).

Sample Setup:
-------------

- update SystemControllerDemo.jad file if needed.
  Change value or remove the following property(s):
    AppIMletBtn1, AppIMletBtn2, AppIMletBtn3 (Application IMlet names to be controlled)

- make sure System IMlet is signed

- install SystemControllerDemo.jad/jar

- install Application IMlet(s) - provided "stubbed" (StubApp1.jar, StubApp2.jar, StubApp3.jar) or others.
  It may be done later when System IMlet is already running.

- start System IMlet or restart/reboot VM/platform

- to see the sample's log:
  open remote terminal console (e.g. putty) and connect to the target device
  (use IP-address of the target device, port 23)

Behavior Description:
---------------------

- System IMlet is started automatically after VM/platform restarted/rebooted.
  SystemControllerDemo.jad has property to switch auto-start on/off:
    Oracle-MIDlet-Autostart: 1
  NOTE: There is a delay of the auto-start (by default ~30 sec).
        The delay is controlled by the string in jwc_properties.ini :
          com.oracle.midp.ams.headless.autostart.delaytime = 30000
         (default value 30000, in milliseconds).

- After start System IMlet takes names of Application IMlets from properties
  and maps these applications to corresponding buttons and LEDs.
  (See rules of the mapping below.)
  A property may be missed/empty. In this case it is ignored.
  
- Mapping rules on the Keil F200:
  
    Jad property    Button name      LED name           
    (App IMlet)     on the board     on the board
    ------------    -------------    -------------
    AppIMletBtn1      WAKEUP           PH3
    AppIMletBtn2      TAMPER           PH6
    AppIMletBtn3      USER             PH7

- Mapping rules on ME SDK :

    Jad property    GPIO name                                               GPIO name           
    (App IMlet)     of emulated                                             of emulated 
                    button                                                  LED  	
    ------------    -----------------------------------------------         -----------
    AppIMletBtn1    Pin 5 (BUTTON 1)(high value = "pressed" button)           LED 1
    AppIMletBtn2    Pin 6 (BUTTON 2)(low value = "pressed" button)            LED 2
    AppIMletBtn3    Pin 7 (BUTTON 3)(low value = "pressed" button)            LED 3

	
- When started and later, during running, System IMlet checks the status of every Application IMlet
  and shows it via corresponded LED (described above).
  A LED behavior:
    - off/low      : Application IMlet is not installed (or property is absent/empty).
    - on/high      : Application IMlet is installed but not running.
    - flashing     : Application IMlet is running.

- System IMlet listens for the buttons (described above).
  Every button press toggles the lifecycle of the corresponded Application IMlet
  between "running" and "not running". Unpress of the button does not make any change.
  If corresponded Application IMlet is not installed then the button press is ignored.

- System IMlet uses watchdog to reset/reboot the system in case of hang or critical failure.
  Watchdog timeout is 30 sec.

- System IMlet logs Application IMlets lifecycle changes and other internal events using Logging API.

Signing Instruction (NetBeans):
-------------------------------

- Right-click on a SystemControllerDemo project, choose Properties, and select the Signing subcategory
  of the Build category.

- Click Open Keystore Manager ...

- The Keystores Manager opens. Click Add Keystore, choose Add Existing Keystore, browse to the location
  of the keystore and select the SystemControllerDemo.ks file in SystemControllerDemo project's folder,
  and then click OK.

- The SystemControllerDemo.ks appears in the Keystores list. Choose SystemControllerDemo.ks in
  the Keystores list, click Unlock Keystore, enter "password" as a Password, and then click OK.

- The key appears in the Keys list. Choose the key in the Keys list, click Unlock ..., do not enter
  anything as a Password, just click OK.
  
- Click Export... The Export Key into Java ME SDK/Platform/Emulator opens. Choose an EmbeddedDevice as
  an Emulator, click Export, and then click Close.
  
- Close the Keystores Manager.

- Choose the SystemControllerDemo.ks as a Keystore, and choose the key as an Alias, and then click OK.

- Finally, exported key should be added to appropriate client by editing of the security policy.