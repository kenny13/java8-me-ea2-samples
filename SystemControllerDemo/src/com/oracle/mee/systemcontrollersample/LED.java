/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.PeripheralTypeNotSupportedException;
import com.oracle.deviceaccess.gpio.GPIOPin;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

 /**
 * GPIO LED. Opens and closes peripheral device GPIO pin specified by name.
 */
public class LED {
    
    private final IMletController imletController = IMletController.getInstance();
    private GPIOPin led;
    private int imlID;
    private String name;

    LED(String name, int imlID) {
        this.name = name;
        this.imlID = imlID;
    }

    public void open() throws IOException, PeripheralTypeNotSupportedException,
            PeripheralNotFoundException, UnavailablePeripheralException {
        led = (GPIOPin) PeripheralManager.open(name, GPIOPin.class);
    }

    /**
     * Lights on/of or blinks the LED. Sets value to the LED by DAAPI GPIO.
     */
    public void changeValue() {
        try {
            boolean prevState = led.getValue();
            if (imletController.isIMletRunning(imlID)) {
                led.setValue(!prevState);

            } else {
                if (imletController.getIMletSuite(imlID) != null) {
                    led.setValue(true);
                } else {
                    led.setValue(false);
                }
            }
        } catch (UnavailablePeripheralException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        }
    }

    public void close() throws IOException, UnavailablePeripheralException {
        if (led != null) {
            led.setValue(false);
            led.close();
        }
    }
}
