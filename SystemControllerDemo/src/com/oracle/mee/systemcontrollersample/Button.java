/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.PeripheralTypeNotSupportedException;
import com.oracle.deviceaccess.gpio.GPIOPin;
import com.oracle.deviceaccess.gpio.PinEvent;
import com.oracle.deviceaccess.gpio.PinListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

/**
 * GPIO button. Opens and closes peripheral device GPIO pin specified by name.
 * Sets listener for this button.
 */
public class Button implements PinListener {

    private static final String PREFIX = "Button: ";
    private final IMletController imletController = IMletController.getInstance();
    private GPIOPin btn;
    private int imlID;
    private String name;

    Button(String name, int imlID) {
        this.name = name;
        this.imlID = imlID;
    }

    public void open() throws IOException, PeripheralTypeNotSupportedException,
            PeripheralNotFoundException, UnavailablePeripheralException {
        btn = (GPIOPin) PeripheralManager.open(name, GPIOPin.class);
        btn.setInputListener(this);
    }

    public void close() throws IOException {
        if (btn != null) {
            btn.close();
        }
    }

    /**
     * Listens the button value changes and starts/stops corresponding
     * Application IMlet using IMlet Controller.
     */
    public void valueChanged(PinEvent pe) {
            try {
                if (!imletController.isIMletRunning(imlID)) {
                    imletController.runIMlet(imlID);
                } else {
                    imletController.stopIMlet(imlID);
                }
            } catch (IllegalArgumentException ex) {
                Logger.getGlobal().log(Level.WARNING, PREFIX + ex.getMessage());
            }
    }
}
