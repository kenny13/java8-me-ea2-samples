/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.microedition.swm.Task;
import javax.microedition.swm.TaskListener;
import javax.microedition.swm.TaskStatus;


/**
 * Processes tasks (applications) status updates. AMS API is used to listen
 * Application IMlets lifecycle events.
 *
 * @see IMletController
 */
public class SuiteControllerTaskListener implements TaskListener {

    private static final String PREFIX = "TaskListener: ";
    private static final String IMLET_STOP = " stopped.";
    private static final String IMLET_TERMINATED = " terminated.";    
    private static final String IMLET_START = " started.";
    private static final String IMLET_RUNNING = " running.";
    private static final String IMLET_PAUSED = " paused.";
    private static final String IMLET_EXITED_FATAL = " exited with an error.";    
    private static final String IMLET_UNDEFINED_STATE = " in an undefined state.";    
    
    private final IMletController imletController = IMletController.getInstance();
    

    private String switchMessage(TaskStatus status) {
        String message = IMLET_UNDEFINED_STATE;
        if(status == TaskStatus.STARTING) {
            message = IMLET_START;
        } if(status == TaskStatus.RUNNING) {
        	message = IMLET_RUNNING;
        } if(status == TaskStatus.PAUSED) {
        	message = IMLET_PAUSED;
        } if(status == TaskStatus.EXITED_REGULAR) {
        	message = IMLET_STOP;
        } if(status == TaskStatus.EXITED_TERMINATED) {
        	message = IMLET_TERMINATED;
        } if(status == TaskStatus.EXITED_FATAL) {
        	message = IMLET_EXITED_FATAL;
        }
        
        return message;
    }

    /**
     * Notifies a listener when a task changes its status. Determines if a 
     * suite belongs to the list of IMlets managed by the System Controller.
     *
     * @param ti finished task
     * @param i task exit code
     */    
	public void notifyStatusUpdate(Task task, TaskStatus status) {
        for (int j = 0; j < imletController.getIMletsSuiteCount(); j++) {
            if (imletController.getIMletSuite(j) != null) {
                if (task.getSuite().getName().equals(imletController.getIMletSuite(j).getName())) {
                    Logger.getGlobal().log(Level.INFO,
                            PREFIX + imletController.getIMletSuite(j).getName() + switchMessage(status));
                    break;
                }
            }
        }		
	}
}
