/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.PeripheralTypeNotSupportedException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;

/**
 * Controls Application IMlets execution. Uses hardware buttons to run and stop
 * Application IMlets.
 *
 * @see IMletController
 */
public class ButtonListener{

    private static final String PREFIX = "ButtonListener: ";
    
    private static final String BUTTON1_NAME = "BUTTON 1";
    private static final String BUTTON2_NAME = "BUTTON 2";
    private static final String BUTTON3_NAME = "BUTTON 3";
    
    private Button btn1;
    private Button btn2;
    private Button btn3;
    
    /**
     * Opens GPIO buttons with specified names and maps them to corresponding
     * Application IMlets. Uses DAAPI GPIO.
     */
    public boolean initialize() {
        boolean isInitialized = false;
        try {
            btn1 = new Button(BUTTON1_NAME, IMletController.IML1_ID);
            btn2 = new Button(BUTTON2_NAME, IMletController.IML2_ID);
            btn3 = new Button(BUTTON3_NAME, IMletController.IML3_ID);
            btn1.open();
            btn2.open();
            btn3.open();
            isInitialized = true;
        } catch (PeripheralTypeNotSupportedException ex) {
            ex.printStackTrace();
        } catch (PeripheralNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (!isInitialized) {
            uninitialize();
            return isInitialized;
        } else {
            return isInitialized;
        }
    }

    public void uninitialize() {
        try {
            if (btn1 != null) {
                btn1.close();
            }
            if (btn2 != null) {
                btn2.close();
            }
            if (btn3 != null) {
                btn3.close();
            }
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, PREFIX + ex.getMessage());
        }
    } 
}