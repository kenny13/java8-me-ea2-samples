/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.microedition.swm.Suite;
import javax.microedition.swm.SuiteManager;
import javax.microedition.swm.SuiteType;
import javax.microedition.swm.Task;
import javax.microedition.swm.TaskManager;
import javax.microedition.swm.ManagerFactory;


/**
 * Implements control over Application IMlets executing. Uses AMS API
 * <code>TaskManager</code> and
 * <code>SuiteStoreManager</code> for managing tasks and suites.
 *
 * @see SuiteListener
 * @see TaskListener
 */
public class IMletController {

    /**
     * ID of 1st Application IMlet which is controlled with Button 1.
     */
    public static final int IML1_ID = 0;
    /**
     * ID of 2nd Application IMlet which is controlled with Button 2.
     */
    public static final int IML2_ID = 1;
    /**
     * ID of 3nd Application IMlet which is controlled with Button 3.
     */
    public static final int IML3_ID = 2;
    /**
     * Data about Application IMlets that are installed.
     */
    private static Suite[] imlets;
    
    private static volatile IMletController instance;
    
    private static final String PREFIX = "IMletController: ";
    private static final String IMLT_INITIALIZED = " initialized.";
    private static final String IMLT_NOT_INITIALIZED = " not initialized.";
    
    private SuiteManager suiteStore;
    private TaskManager taskManager;
    private List tasks;
    
    private IMletController() {}
    
    public static synchronized IMletController getInstance() {
        if (instance == null) {
            instance = new IMletController();
        }
        return instance;
    }

    /**
     * Creates
     * <code>StoreManager</code> to manage installing/removing suites and
     * <code>TaskManager</code> to manage executing tasks.
     */
	public boolean start() {
		boolean isStarted = false;
		try {
			suiteStore = ManagerFactory.getSuiteManager();
			suiteStore.addSuiteListener(new SuiteControllerListener());
			taskManager = ManagerFactory.getTaskManager();
			imlets = new Suite[SystemControllerIMlet.getIMletCount()];
			for (int i = 0; i < imlets.length; i++) {
				if (SystemControllerIMlet.getIMletName(i) != null
						&& SystemControllerIMlet.getIMletName(i).length() > 0) {
					initializeIMlet(i);
				}
			}
			taskManager.addStatusListener(new SuiteControllerTaskListener());
			isStarted = true;
		} catch (Exception ex) {
			Logger.getGlobal().log(Level.SEVERE, PREFIX + ex.getMessage());
		}

		return isStarted;
	}

    /**
     * Stops listening events for
     * <code>StoreManager</code> and
     * <code>TaskManager</code>.
     */
    public void stop() {
        if (suiteStore != null) {
            suiteStore.addSuiteListener(null);
        }
        if (taskManager != null) {
            taskManager.addStatusListener(null);
        }
    }

    /**
     * Initializes an Application IMlet.
     *
     * @param id Application IMlet ID
     */
    public void initializeIMlet(int id) {
        try {
            List appSuites = suiteStore.getSuites(SuiteType.APPLICATION);
            boolean stop = false;
            for (int i = 0; i < appSuites.size() && !stop; i++) {
                if (SystemControllerIMlet.getIMletName(id).equals(((Suite)appSuites.get(i)).getName())) {
                    stop = true;
                    imlets[id] = (Suite)appSuites.get(i);
                    Logger.getGlobal().log(Level.INFO,
                            PREFIX + imlets[id].getName() + IMLT_INITIALIZED);
                }
            }
            if (!stop) {
                imlets[id] = null;
                Logger.getGlobal().log(Level.INFO,
                        PREFIX + SystemControllerIMlet.getIMletName(id) 
                        + IMLT_NOT_INITIALIZED);
            }
        } catch (SecurityException ex) {
            Logger.getGlobal().log(Level.SEVERE, PREFIX + ex.getMessage());
        }
    }

    /**
     * Starts an Application IMlet execution.
     *
     * @param id Application IMlet ID
     */
	public void runIMlet(int id) {
		if (imlets[id] != null) {
			taskManager.startTask(imlets[id], imlets[id].getMIDlets().next());
		} else {
			throw new IllegalArgumentException();
		}
	}

    /**
     * Stops an Application IMlet execution.
     *
     * @param id Application IMlet ID
     */
    public void stopIMlet(int id) {
        if (imlets[id] != null) {
            tasks = taskManager.getTaskList(false);
            for (int i = 0; i < tasks.size(); i++) {
                if ((((Task)tasks.get(i)).getSuite().getName().equals(imlets[id].getName()))) {
                    try {
                    	taskManager.stopTask((Task)tasks.get(i));
                    } catch (SecurityException ex) {
                        Logger.getGlobal().log(Level.SEVERE, PREFIX + ex.getMessage());
                    }
                    break;
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Checks execution state of an Application IMlet.
     *
     * @param iid Application IMlet ID
     * @return <code>true</code> if an Application IMlet with <code>id</code>
     * is running
     */
    public boolean isIMletRunning(int id) {
        if (imlets[id] != null) {
            tasks = taskManager.getTaskList(false);
            boolean stop = false;
            for (int i = 0; i < tasks.size() && !stop; i++) {
                if (((Task)tasks.get(i)).getSuite().getName().equals(imlets[id].getName())) {
                    stop = true;
                }
            }
            return stop;
        }
        return false;
    }
    
    public Suite getIMletSuite(int id) {
        return imlets[id];
    }
    
    public int getIMletsSuiteCount(){
        return imlets.length;
    }
}
