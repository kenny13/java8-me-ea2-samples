/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import javax.microedition.swm.SuiteListener;
import javax.microedition.swm.SuiteManagementTracker;
import javax.microedition.swm.SuiteState;


/**
 * Processes suite store updates. AMS API is used to listen IMlets
 * install/remove events.
 *
 * @see ImletController
 */
public class SuiteControllerListener implements SuiteListener {

    private final IMletController imletController = IMletController.getInstance();

    /**
     * Notifies a listener that a suite has been changed. Determines if a suite
     * belongs to the list of IMlets managed by the System Controller.
     *
     * @param si installed suite
     */    	
	public void notifySuiteStateChanged(SuiteManagementTracker tracker,
			SuiteState state) {
        for (int i = 0; i < imletController.getIMletsSuiteCount(); i++) {
            if(tracker.getSuite() != null) {
                if (tracker.getSuite().getName().equals(SystemControllerIMlet.getIMletName(i))) {
	            imletController.initializeIMlet(i);
	            break;
	        }
	    }
	}
    }	
}
