/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.PeripheralTypeNotSupportedException;
import com.oracle.deviceaccess.watchdog.WatchdogTimer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Implements LEDs blinking to indicate whether an Application IMlet is
 * executing or not. This task starts in the
 * <code>SystemControllerImlet</code> and controls the blinking of the LEDs:
 * <ul> <li>LED is off when an Application IMlet is not installed 
 * (or property is absent/empty).
 * <li>LED is on when an Application IMlet is installed but not running.
 * <li>LED flashes when an Application IMlet is running. </ul> 
 * Also this task creates <code>WatchdogTimer</code> and controls it.
 */
public class StateTask extends TimerTask {

    private static final int PERIOD_MS = 1000;
    private static final int WDT_PERIOD_MS = 30000;
    
    private static final String LED1_NAME = "LED 1";
    private static final String LED2_NAME = "LED 2";
    private static final String LED3_NAME = "LED 3";
    
    private static final String WDT = "WDG";
    
    private LED led1;
    private LED led2;
    private LED led3;
    
    private WatchdogTimer wdt;
    
    private Timer timer;
    
    /**
     * Opens GPIO LEDs with specified names and maps them to corresponding
     * Application IMlets. Also starts watchdog and timer.
     */
    public boolean start() { 
        boolean isStarted = false;
        try {
            led1 = new LED(LED1_NAME, IMletController.IML1_ID);
            led2 = new LED(LED2_NAME, IMletController.IML2_ID);
            led3 = new LED(LED3_NAME, IMletController.IML3_ID);
            led1.open();
            led2.open();
            led3.open();
            wdt = (WatchdogTimer) PeripheralManager.open(WDT, WatchdogTimer.class);
            wdt.start(WDT_PERIOD_MS);
            timer = new Timer();
            timer.schedule(this, 0, PERIOD_MS);
            isStarted = true;
        } catch (PeripheralTypeNotSupportedException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        } catch (PeripheralNotFoundException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        } catch (UnavailablePeripheralException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        }
        return isStarted;
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }
        try {
            led1.close();
            led2.close();
            led3.close();
           
            if (wdt != null) {
                wdt.stop();
                wdt.close();
            }
        } catch (UnavailablePeripheralException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        }
    }

    /**
     * Worker thread that actually performs LEDs blinking & refreshes
     * WatchdogTimer.
     */
    public void run() {
        try {
            wdt.refresh();
        } catch (UnavailablePeripheralException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        }
        led1.changeValue();
        led2.changeValue();
        led3.changeValue();
    }
}
