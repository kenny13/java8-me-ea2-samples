/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.systemcontrollersample;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.midlet.MIDlet;

/**
 * Performs application startup and shutdown. Creates, starts and initializes
 * one instance of each of the following classes:<ul> <li>IMletController
 * <li>ButtonListener <li>StateTask <li>LoggingHandler</ul>
 * 
 * <p><b>Target Platforms</b>: emulator and KEIL MCBSTM32F200 development board.</p>
 * <p>For more information, please see the accompanying readme.txt file</p>
 *
 * @see IMletController
 * @see ButtonListener
 * @see StateTask
 * @see LoggingHandler
 */
public class SystemControllerIMlet extends MIDlet {

    /**
     * Names of Application IMlets that controlled by GPIO buttons. Names are
     * obtained from
     * <code>AppIMletBtn1</code>,
     * <code>AppIMletBtn2</code>,
     * <code>AppIMletBtn3</code> JAD properties.
     */
    private static String[] imlets;
    
    private static final int imletsCount = 3;
    
    private ButtonListener buttonListener;
    private IMletController imletController;
    private StateTask stateTask;
    private LoggingHandler loggerHandler = LoggingHandler.getInstance();
    
    public static int getIMletCount() {
        return imletsCount;
    }
    
    public static String getIMletName(int id) {
        return imlets[id];
    }
    
    
    public void startApp() {
        loggerHandler.start();

        Logger.getGlobal().log(Level.INFO, "*********************************");
        Logger.getGlobal().log(Level.INFO, "*   System Controller Sample    *");
        Logger.getGlobal().log(Level.INFO, "*********************************");

        imlets = new String[imletsCount];

        for (int i = 0; i < imletsCount; i++) {
            imlets[i] = getAppProperty("AppIMletBtn" + (i + 1));
        }

        Logger.getGlobal().log(Level.INFO, "Starting IMlet Controller...");
        imletController = IMletController.getInstance();

        if (imletController.start()) {
            Logger.getGlobal().log(Level.INFO, "IMlet Controller start is OK.");
            Logger.getGlobal().log(Level.INFO, "Initializing button listener...");
            buttonListener = new ButtonListener();
            if (buttonListener.initialize()) {
                Logger.getGlobal().log(Level.INFO, "Button listener initialization is OK.");
            }else {
                Logger.getGlobal().log(Level.INFO, "Button listener initialization is failed.");
                notifyDestroyed();
            }
            
            Logger.getGlobal().log(Level.INFO, "Starting state task...");
            stateTask = new StateTask();

            if (stateTask.start()) {
                Logger.getGlobal().log(Level.INFO, "State task start is OK");
            }else {
                Logger.getGlobal().log(Level.INFO, "State task start is failed.");
                notifyDestroyed();
            }

        } else {
            Logger.getGlobal().log(Level.INFO, "IMlet Controller start is failed.");
            notifyDestroyed();
        }
        
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
        if (stateTask != null) {
            stateTask.stop();
        }
        if (buttonListener != null) {
            buttonListener.uninitialize();
        }
        if (imletController != null) {
            imletController.stop();
        }
        if (loggerHandler != null) {
            loggerHandler.stop();
        }
    }
}
