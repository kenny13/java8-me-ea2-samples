/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.dataprocessor;

import com.oracle.mee.datacollectionsample.share.Data;
import com.oracle.mee.datacollectionsample.share.DataProcessor;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

/**
 * Implements <code>DataProcessor</code>. Gets data and redirects
 * it to the Logger.
 * 
 * @see LoggingHandler
 */
public class DataHandler implements DataProcessor{
    
    private final String PREFIX = "Data: "; 
    private final String PULSES_STRING = "Pulses ";
    private final String TEMP_STRING = "Temperatures ";
    private final String TIMESTAMP_STRING = "timestamp = ";
    private final String VALUE_STRING  = " value = ";
    
    /**
     * Process received data and logs them.
     * @param data received data
     */
    public void process(Data data) {
        StringBuffer messageLog = new StringBuffer(TIMESTAMP_STRING + new Date(data.getTimeStamp()).toString());
        int numVal = data.getNumberOfValues();
        for (int i = 0; i < numVal; i++) {
            messageLog.append(VALUE_STRING).append(new Integer(data.getValue(i)).toString());
        }
        switch (data.getType()) {
            case Data.DATA_TYPE_PULSES:
                Logger.getGlobal().log(Level.INFO, PREFIX + PULSES_STRING + messageLog);
                break;
            case Data.DATA_TYPE_TEMPERATURE:
                Logger.getGlobal().log(Level.INFO, PREFIX + TEMP_STRING + messageLog);
                break;
            default:
                break;

        }
    }
}
