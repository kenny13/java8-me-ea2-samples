/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.dataprocessor;

import javax.microedition.midlet.*;

/**
 * Data Processor IMlet gets data from Data Collectors via inter-IMlet 
 * communication. And processes the received data.
 * Creates and starts <code>ProtocolServer</code>.
 * 
 * <p><b>Target Platforms</b>: emulator and KEIL MCBSTM32F200 development board.</p>
 * <p>For more information, please see the accompanying readme.txt file</p>
 * 
 * @see DataHandler
 * @see ProtocolServer
 * @see LoggingHandler
 */
public class DataProcessorIMlet extends MIDlet {
    private ProtocolServer server;
    private LoggingHandler loggerHandler = LoggingHandler.getInstance();
    private boolean isAlreadyStarted = false;
    
    public void startApp() {
        if (!isAlreadyStarted) {
            loggerHandler.start();
        
            System.out.println("*********************************");
            System.out.println("*     Data Processor Sample     *");
            System.out.println("*********************************");
        
            System.out.println("Starting server...");
            server = new ProtocolServer(new DataHandler());
            if (server.start()) {
                System.out.println("Server start is OK.");
            } else {
                System.out.println("Server start is failed.");
                notifyDestroyed();
            }
            isAlreadyStarted = true;
        }
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
        if (server != null) {
            server.stop();
        }
        if (loggerHandler != null) {
            loggerHandler.stop();
        }
    }
}
