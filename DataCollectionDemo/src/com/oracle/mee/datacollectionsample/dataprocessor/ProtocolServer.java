/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.dataprocessor;

import com.oracle.mee.datacollectionsample.share.Data;
import com.oracle.mee.datacollectionsample.share.DataProcessor;
import com.oracle.mee.datacollectionsample.share.Protocol;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.io.DatagramConnection;

/**
 * Listens port for datagram connections from Data Collectors, parses data. 
 * Uses <code>DataProcessor</code> methods for process this data.
 * 
 * @see DataHandler
 */
public class ProtocolServer implements Runnable {
    
    private DatagramConnection receiver;
    private Datagram messageData;
    private volatile boolean shouldRun;
    private final DataProcessor processor;
    
    
    ProtocolServer(DataProcessor processor){
        this.processor = processor;
    }
    /**
     * Start server. Creates datagram connection and starts thread.
     */
    public boolean start() {
        boolean isStarted = false;
        shouldRun = true;
        try {
            receiver = (DatagramConnection) Connector
                    .open("datagram://:" + Protocol.DEFAULT_PORT);        
            new Thread(this).start();
            isStarted = true;
        } catch (IOException ex) {
            shouldRun = false;
            ex.printStackTrace();
        }
        return isStarted;
    }
    
    public void stop() {
        shouldRun = false;
        try {
            if (receiver != null) {
                receiver.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * ProtocolServer thread. Receives data from Data Collectors and parses them.
     */
    public void run() {
        int valueCount;
        while (shouldRun) {
            try {
                messageData = receiver.newDatagram(receiver.getMaximumLength());
                receiver.receive(messageData);
                valueCount = messageData.readInt();
                int type = messageData.readInt();
                long timeStamp = messageData.readLong();
                int values[] = new int[valueCount];
                for (int i = 0; i < valueCount; i++) {
                    values[i] = messageData.readInt();
                }
                try {
                    processor.process(new Data(type, timeStamp, values));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } catch (IOException ex) {
                stop();
            }
        }
    }

}
