/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.spithermometer;

import com.oracle.mee.datacollectionsample.share.ProtocolClient;
import javax.microedition.midlet.*;

/**
 * Data Collector for temperature. Uses external Thermometer connected via SPI bus.
 * Gets temperature and sends measured value to the Data Processor.
 * Creates and starts one instance of <code>SPIChecker</code>.
 * 
 * <p><b>Target Platforms</b>: emulator and KEIL MCBSTM32F200 development board.</p>
 * <p>For more information, please see the accompanying readme.txt file</p>
 *
 * @see SPIChecker
 */
public class SPIThermometerIMlet extends MIDlet {

    private SPIChecker spiChecker;
    private ProtocolClient client;
    private boolean isAlreadyStarted = false;

    public void startApp() {
        if (!isAlreadyStarted) {
            System.out.println("*********************************");
            System.out.println("*     SPI Thermometer Sample    *");
            System.out.println("*********************************");

            System.out.println("Starting ProtocolClient...");
            client = new ProtocolClient();
        
            if (client.open()) {
                System.out.println("Starting SPIChecker...");
                spiChecker = new SPIChecker(client);
                if (spiChecker.start()) {
                    System.out.println("SPIChecker start is OK.");
                } else {
                    System.out.println("SPIChecker start is failed.");
                    notifyDestroyed();
                }
            } else {
                System.out.println("Starting ProtocolClient is failed.");
                notifyDestroyed();
            }
            
            isAlreadyStarted = true;
        }
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
        if (spiChecker != null) {
           spiChecker.stop(); 
        }
        if (client != null) {
            client.close();
        }
    }
}
