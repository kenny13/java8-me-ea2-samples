/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.spithermometer;

import com.oracle.deviceaccess.PeripheralConfig;
import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.PeripheralTypeNotSupportedException;
import com.oracle.deviceaccess.spibus.SPIDevice;
import com.oracle.deviceaccess.spibus.SPIDeviceConfig;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * SPI Thermometer driver. Opens SPI device and gets temperature.
 */
public class Thermometer {  
    private static SPIDevice spiDevice = null;
    private static final int SPIDEVICE_ADDRESS = 0;
    
    /**
     * Opens SPI device Thermometer uses DAAPI SPI.
     */
    public static boolean connect() {
        boolean isConnected = false;
        try {
            SPIDeviceConfig config = new SPIDeviceConfig(PeripheralConfig.DEFAULT,
                   SPIDEVICE_ADDRESS, PeripheralConfig.DEFAULT, 0, PeripheralConfig.DEFAULT, PeripheralConfig.DEFAULT);
            spiDevice = (SPIDevice) PeripheralManager.open(SPIDevice.class, config);
            /* thermometer configuration : SD bit = 0  "continuous conversion mode" */
            byte[] txBuf = { (byte)0x80, (byte)0xF0 };
            spiDevice.begin();
            spiDevice.write(ByteBuffer.wrap(txBuf));
            spiDevice.end();
            isConnected = true;
        } catch (PeripheralTypeNotSupportedException ex) {
            ex.printStackTrace();
        } catch (PeripheralNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return isConnected;
    }
    
    public static void disconnect() {
        try {
            if (spiDevice != null) {
                spiDevice.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Returns current sensor temperature.
     */
    public static int getTemperature() {
        try {
            spiDevice.begin();
            spiDevice.write(0x02);
            int value = spiDevice.read();
            spiDevice.end();
            return decodeMSB( value );
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    private static int decodeMSB(int msb) {
        /* valid only for positive values */
        return msb;
    }
    
    
}
