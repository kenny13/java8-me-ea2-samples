/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.spithermometer;

import com.oracle.mee.datacollectionsample.share.Data;
import com.oracle.mee.datacollectionsample.share.DataProcessor;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Works with Thermometer connected via SPI bus. Periodically reads temperature
 * and calls processing of the current value.
 */
public class SPIChecker extends TimerTask {
    private final DataProcessor processor;
    private static final int PERIOD_MS = 5000;
    private Timer timer;
    private int degree;
    
    SPIChecker(DataProcessor processor){
        this.processor = processor;
    }
    
    /**
     * Connects thermometer and client connection.
     */
    public boolean start() {
        boolean isStarted = false;
        try {
            if (Thermometer.connect()) {
                timer = new Timer();
                timer.schedule(this, 0, PERIOD_MS);
                degree = Thermometer.getTemperature();
                processor.process(new Data(Data.DATA_TYPE_TEMPERATURE, System.currentTimeMillis(),
                        new int[]{degree}));
                isStarted = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isStarted;
    }
    
    /**
     * Invokes when the tracking time interval has expired. Calls processing 
     * of the measured temperature.
     */
    public void run() {
        int newDegree = Thermometer.getTemperature();
        if (newDegree != degree) {
            degree = newDegree;
            try {
                processor.process(new Data(Data.DATA_TYPE_TEMPERATURE,
                        System.currentTimeMillis(),
                        new int[]{degree}));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }
        Thermometer.disconnect();
    }    
}
