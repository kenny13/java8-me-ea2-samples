/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */

package com.oracle.mee.datacollectionsample.pulsecounter;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.counter.CountingEvent;
import com.oracle.deviceaccess.counter.CountingListener;
import com.oracle.deviceaccess.counter.PulseCounter;
import com.oracle.mee.datacollectionsample.share.Data;
import com.oracle.mee.datacollectionsample.share.DataProcessor;
import java.io.IOException;

/**
 * Uses DAAPI Pulse Counter. Counts number of pulses which happens
 * during specified time interval <code>CountingIntervel</code>.
 */
public class PulseCounterListener implements CountingListener{
    private PulseCounter counter;
    private static final int counterID = 201;
    /**
     * Number of pulses happened since start of this IMlet.
     */
    private int count;
    private final DataProcessor processor;
    
    PulseCounterListener(DataProcessor processor){
        this.processor = processor;
    }

    public void failed(Throwable t, PulseCounter pulseCounter) {
    }

    /**
     * Opens pulse counter.
     */
    public boolean start() {
        boolean isStarted = false;
        try {
            counter = (PulseCounter) PeripheralManager.open(counterID);
            counter.startCounting(-1, PulseCounterIMlet.getCountingInterval(), this);
            count = counter.getCount();
            processor.process(new Data(Data.DATA_TYPE_PULSES, System.currentTimeMillis(), new int[]{count}));
            isStarted = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (!isStarted) {
            stop();
            return isStarted;
        } else {
            return isStarted;
        }
    }
    
    /**
     * Is invoked when the counting time interval has expired. Calls processing 
     * of the current number of pulses.
     */
    public void countValueAvailable(CountingEvent ce) {
        int newCount = count + ce.getValue();
        if (newCount != count) {
            count = newCount;
            try {
                processor.process(new Data(Data.DATA_TYPE_PULSES, 
                        System.currentTimeMillis(), new int[]{count}));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void stop() {
        try {
            if (counter != null) {
                counter.stopCounting();
                counter.close();
            }
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
