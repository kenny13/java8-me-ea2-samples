/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.pulsecounter;

import com.oracle.mee.datacollectionsample.share.ProtocolClient;
import javax.microedition.midlet.*;

/**
 * Data Collector for number of pulses. Counts number of pulses and
 * periodically sends the current number to Data Processor. Uses DAAPI Pulse Counter.
 * Creates and starts one instance of <code>PulseCounterListener</code>.
 * 
 * <p><b>Target Platforms</b>: emulator and KEIL MCBSTM32F200 development board.</p>
 * <p>For more information, please see the accompanying readme.txt file</p>
 *
 * @see PulseCounterListener
 */
public class PulseCounterIMlet extends MIDlet {
    private PulseCounterListener pCounter;
    private static int CountingInterval; 
    private ProtocolClient client;
    private boolean isAlreadyStarted = false;
    
    /**
     * Gets JAD property CountingInterval with time period for
     * <code>PulseCounterListener</code>.
     */
    public static int getCountingInterval() {
        return CountingInterval;
    }
    
    public void startApp() {
        if (!isAlreadyStarted) {
            CountingInterval = Integer.parseInt(getAppProperty("CountingInterval"));

            System.out.println("*********************************");
            System.out.println("*      Pulse Counter Sample     *");
            System.out.println("*********************************");
        
            System.out.println("Starting ProtocolClient...");
            client = new ProtocolClient();
            if (client.open()) {
                System.out.println("ProtocolClient is OK.");
                System.out.println("Starting PulseCounterListener...");
                pCounter = new PulseCounterListener(client);
                if (pCounter.start()) {
                    System.out.println("PulseCounterListener start is OK.");
                } else {
                    System.out.println("PulseCounterListener start is failed.");
                    notifyDestroyed();
                }
            } else {
                System.out.println("ProtocolClient is failed.");
                notifyDestroyed();
            }
        
            isAlreadyStarted = true;
        }
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
        if (pCounter != null) {
            pCounter.stop();
        }
        if (client != null) {
            client.close();
        }
    }
}
