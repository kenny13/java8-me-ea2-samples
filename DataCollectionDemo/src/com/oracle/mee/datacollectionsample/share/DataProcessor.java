/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.share;

/**
 * Data Processor Interface. Transfers data from Data Collectors to Data Processor.
 */
public interface DataProcessor {
    
    /**
     * Processes data. 
     * @param data object Data
     * @throws Exception
     */
    public void process(Data data)  throws Exception;

    
}
