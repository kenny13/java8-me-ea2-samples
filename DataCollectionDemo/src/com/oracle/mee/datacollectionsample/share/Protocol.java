/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.share;

/**
 * Represents inter-IMlet protocol. This is a simple internal protocol on top of
 * datagrams used for transferring data from Data Collector(s) to Data Processor.
 * The protocol packet consists of:
 * - int numVal         : number of values (not including dataType and timeStamp)
 * - int dataType       : type of data
 * - long timeStamp     : timestamp
 * - int values[numVal] : data values 
 * One protocol packet must fit into one datagram 
 */
public class Protocol {
    /**
     * Datagram connection port used by client to connect to the server.
     */
    public static final int DEFAULT_PORT = 8080;  
}
