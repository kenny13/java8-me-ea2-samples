/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.share;

import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.io.DatagramConnection;

/** Inter-IMlet communication Client. Implements <code>DataProcessor</code> 
 * interface. Packs data into datagrams and sends to a server.
 * 
 * @see DataProcessor
 */
public class ProtocolClient implements DataProcessor {

    private DatagramConnection sender;
    private Datagram messageData;
    private final static String INVALID_SIZE = "Invalid size of sending message.";
    private final int intSize   = 4;

    /**
     * Opens datagram connection.
     */
    public boolean open() {
        boolean isOpened = false;
        try {
            sender = (DatagramConnection) Connector
                    .open("datagram://localhost:" + Protocol.DEFAULT_PORT);
            isOpened = true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return isOpened;
    }
    
    /**
     * Closes datagram connection.
     */
    public void close() {
        try {
            if (sender != null) {
                sender.close();
            } 
        } catch (IOException ex) {
            ex.printStackTrace();
        }      
    }
    
    /**
     * Packs data to datagram packet and sends it to a server.
     * One protocol packet must fit into one datagram.
     * @param data sent data
     * @throws Exception 
     */
    public void process(Data data)  throws Exception {
        try {
            messageData = sender.newDatagram(sender.getMaximumLength());
            messageData.reset();
            // (size of Data + size of numValues) must not exceed maximum datagram size 
            if (data.getSize() + intSize > sender.getMaximumLength()) {
                throw new Exception(INVALID_SIZE);
            }
            
            int numValues = data.getNumberOfValues();
            messageData.writeInt(numValues);
            messageData.writeInt(data.getType());
            messageData.writeLong(data.getTimeStamp());
            
            for (int i=0;i<numValues;i++) {
                messageData.writeInt(data.getValue(i));
            }
            sender.send(messageData);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
