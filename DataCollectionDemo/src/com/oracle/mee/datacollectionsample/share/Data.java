/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.datacollectionsample.share;

/**
 * Represents data from peripheral devices/sensors. Consist of type of data,
 * timestamp and data values.
 */
public class Data {
    
    /**
     * Data type Id for number of pulses.
     */
    public static final int DATA_TYPE_PULSES       = 0x01;
    /**
     * Data type Id for temperature value.
     */
    public static final int DATA_TYPE_TEMPERATURE  = 0x02;
    
    //add here more data types when needed
    
    private final int intSize   = 4;
    private final int longSize  = 8;
    
    private int type;
    private long timeStamp;
    private int values[];
    
    public Data(int type, long timeStamp, int values[]){
        this.type = type;
        this.timeStamp = timeStamp;
        this.values = values;
    }
    
    public int getType(){
        return type;
    }
    
    public long getTimeStamp(){
        return timeStamp;
    }
    
    public int getValue(int id) {
        return values[id];
    }
    
    public int getNumberOfValues(){
        return values.length;
    }
    
    /**
     * Calculates size of object: size of type + size of timeStamp + size of values[]
     * @return data size
     */
    public int getSize(){    
        return intSize + longSize + values.length * intSize;
        
    }
}
