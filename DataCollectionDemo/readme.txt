#
# Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
#

Data Collection Sample
----------------------

Overview:
---------

This sample demonstrates the following functionality:
- MVM
- Inter-IMlet communication using local datagrams
- DAAPI pulse counter
- DAAPI SPI
- Logging API

The sample consists of:
- Data Collector IMlet(s) which read data from peripheral devices using DAAPI and send it to Data Processor.
    Current version of the sample includes the following Data Collectors:
      -- Pulse Counter IMlet : reads and sends current number of counted pulses.
      -- SPI Thermometer IMlet (does NOT work on ME SDK) : reads and sends current temperature.
- Data Processor IMlet which receive data from Data Collector(s) and "process" it.

A simple internal protocol on top of datagrams is used for transffering data from Data Collectors to Data Processor.
It is assumed data is not critical. Data may be lost.
Data includes:
  - type of data (pulses, temperature, etc.)
  - timestamp
  - value(s) of data

Sample Setup:
-------------

- Adjust application-specific JAD properties, if needed (see Behavior Description)

- Install all provided IMlets.

- Start all provided IMlets.

- to see the sample's log:
  open remote terminal console (e.g. putty) and connect to the target device
  (use IP-address of the target device, port 23)

Behavior Description:
---------------------

- Data Processor IMlet:
  -- listens datagram port
  -- receives and parses data
  -- logs received data using Logging API

- any Data Collector IMlet:
  -- periodically and/or by event and/or by request reads data from a peripheral device/sensor/pin/bus/etc.
  -- sends data via the simple internal protocol

- Pulse Counter IMlet (Data Collector):
  -- counts absolute number of pulses since the IMlet (re)start
  -- uses DAAPI pulse counter
  -- checks current absolute number of pulses periodically.
     The period is defined by "CountingInterval" JAD property (by default: 5000 ms).
  -- if absolute number of pulses is changed during a period then sends the new value to Data Processor


- SPI Thermometer IMlet (Data Collector):
  -- requires external Digital Thermometer DS1722 connected via SPI bus
  -- uses DAAPI SPI
  -- reads current temperature periodically.
     The period is hardcoded to 5 sec.
  -- if new temperature value (degrees on Celsius) differs from the previous one then sends the new value to Data Processor.