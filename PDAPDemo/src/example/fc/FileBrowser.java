/*
 *
 * Copyright (c) 2009, 2013, Oracle and/or its affiliates. All rights reserved.

 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Oracle Corporation nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package example.fc;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemListener;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.io.ServerSocketConnection;
import javax.microedition.midlet.MIDlet;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import com.oracle.clilib.*;


/**
 * Demonstration of IMlet for File Connection API. This IMlet implements simple file browser for the filesystem 
 * available to the Java ME applications. 
 * 
 * <p>User interaction is handled by command line interface (CLI). To access the CLI you have to start the IMlet 
 * and connect to the port 5001 in passive mode using some terminal application. A command line opens where you can 
 * browse the device's file system. You can use the following commands:
 * <ul>
 *     <li><b>cd</b> - change directory</li>
 *     <li><b>ls</b> - list information about the files for the current directory</li>
 *     <li><b>new</b> - create new file or directory</li>
 *     <li><b>prop</b> - show properties of a file</li>
 *     <li><b>rm</b> - remove the file</li>
 *     <li><b>view</b> -view a file's content</li>
 * </ul></p>
 * 
 * <p><b>Target Platforms</b>: emulator, CLDC/MEEP enabled boards that supports File Connection optional package of JSR 75.</p> 
 */
public class FileBrowser extends MIDlet implements FileSystemListener {
    private static final String[] monthList =
        {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    /* special string denotes upper directory */
    private static final String UP_DIRECTORY = "..";
    
    /* special string that denotes upper directory accessible by this browser.
     * this virtual directory contains all roots.
     */
    private static final String MEGA_ROOT = "/";

    /* separator string as defined by FC specification */
    private static final String SEP_STR = "/";

    /* separator character as defined by FC specification */
    private static final char SEP = '/';
    private String currDirName;
    
    private static final int DEFAULT_LISTEN_PORT = 5001;

    public FileBrowser () {
        currDirName = MEGA_ROOT;
        FileSystemRegistry.addFileSystemListener (this);
    }

    public void startApp () {
        // Checking port availability
        try {
            ServerSocketConnection serverSocketConnection = (ServerSocketConnection) Connector.open("socket://:"
                    + DEFAULT_LISTEN_PORT);
            serverSocketConnection.close();
        } catch (IOException e) {
            System.out.println("Error: port " + DEFAULT_LISTEN_PORT + " is busy!");
            destroyApp(true);
            return;
        }
        
        try {
            CLI.getRegistry().addCommand(new ListCmd());
            CLI.getRegistry().addCommand(new ChangeDir());
            CLI.getRegistry().addCommand(new ShowFile());
            CLI.getRegistry().addCommand(new Property());
            CLI.getRegistry().addCommand(new NewCmd());
            CLI.getRegistry().addCommand(new DeleteCmd());

            Shell shell = CLI.createTcpShell(DEFAULT_LISTEN_PORT);
            shell.setWelcomeMsg("PDAPDemo, FileBrowser\r\n"
                    + "Copyright (c) 2012, Oracle\r\n");
            shell.start();
        }
        catch (Exception e) {
            e.printStackTrace ();
        }
    }

    public void pauseApp () {
    }

    public void destroyApp (boolean cond) {
        FileSystemRegistry.removeFileSystemListener (this);
        notifyDestroyed ();
    }

    /**
     * Show file list in the current directory .
     */
    void showCurrDirNow (OutputWriter writer) {
        Enumeration e;
        FileConnection currDir = null;
        int files = 0;
        int dirs = 0;

        try {
            if (MEGA_ROOT.equals (currDirName)) {
                e = FileSystemRegistry.listRoots ();
            }
            else {
                currDir = (FileConnection) Connector.open ("file://localhost" + currDirName);
                e = currDir.list ();
            }

            while (e.hasMoreElements ()) {
                String fileName = (String) e.nextElement ();

                if (fileName.charAt (fileName.length () - 1) == SEP) {
                    // This is directory
                    writer.line("<DIR>   "+fileName);
                    dirs++;
                }
                else {
                    // this is regular file
                    writer.line("<FILE>  "+fileName);
                    files++;
                }
            }

            if (currDir != null) {
                currDir.close ();
            }
            writer.line("    "+currDirName+"  "+dirs+" dirs and "+files+" files\r\n");
        }
        catch (IOException ioe) {
            ioe.printStackTrace ();
        }
        catch (SecurityException se) {
            writer.line("<<ls,FAIL,You are not authorized to access the restricted API\r\n");
        }
    }

    /**
     * change the current directory
     */
    void traverseDirectory (String fileName, OutputWriter writer) {
        /* In case of directory just change the current directory
         */        
        String tmpCurrDir = currDirName;
        if (fileName.startsWith(MEGA_ROOT)) {
            tmpCurrDir = MEGA_ROOT;
            fileName = fileName.substring(MEGA_ROOT.length());
        }
        
        int separatorIndex;
        if (fileName.length() > 0 && !fileName.endsWith(SEP_STR)) {
                fileName = fileName + SEP_STR;
        }
        
        while ((separatorIndex = fileName.indexOf(SEP_STR)) != -1) {
            String dirName = fileName.substring(0, separatorIndex);
            if (dirName == null || dirName.equals("")) {
                writer.line("<<cd,FAIL,bad format\r\n");
                return;
            }
            fileName = fileName.substring(separatorIndex + 1);
            
            if (dirName.equals(UP_DIRECTORY)) {
                if (tmpCurrDir.equals(MEGA_ROOT)) {
                    // cannot go up from MEGA_ROOT
                    writer.line("<<cd,FAIL,cannot go up from root\r\n");
                    return;
                } else {
                    // Go up one directory
                    int i = tmpCurrDir.lastIndexOf (SEP, tmpCurrDir.length () - 2);
                    if (i != -1) {
                        tmpCurrDir = tmpCurrDir.substring (0, i + 1);
                    } else {
                        tmpCurrDir = MEGA_ROOT;
                    }
                }
            } else {
                tmpCurrDir = tmpCurrDir + dirName + SEP_STR;
                try {
                    FileConnection fc = (FileConnection) Connector.open ("file://localhost" + tmpCurrDir);
                    if (!fc.exists () || !fc.isDirectory()) {
                        writer.line("<<cd,FAIL,no such directory\r\n");
                        return;
                    }
                } catch (Exception e) {
                    writer.line("<<cd,FAIL,Cannot access directory " + tmpCurrDir
                        + "\r\nException: "
                        + e.getMessage() + "\r\n");
                    return;
                }
            }
        }
        
        currDirName = tmpCurrDir;
    }

    /**
     * show file
     */
    void showFile (String fileName, OutputWriter writer) {
        FileConnection fc = null;
        InputStream fis = null;
        try {
            fc = (FileConnection) Connector.open ("file://localhost" + currDirName + fileName);

            if (!fc.exists ()) {
                throw new IOException ("File does not exists");
            }

            if (fc.isDirectory ()) {
                throw new IOException ("it is directory");
            }

            fis = fc.openInputStream ();
            byte[] b = new byte[1024];

            writer.line("<<view,==START==\r\n");
            int length = 0;
            while ((length = fis.read (b, 0, 1024)) > 0) {
                String str = new String (b, 0, length);
                writer.line(str);               
            }
            writer.line("\r\n\r\n<<view,==EOF==\r\n<<view,OK\r\n");
        } catch (Exception e) {
            writer.line("<<view,FAIL,Cannot access file " + fileName
                    + " in directory " + currDirName + "\r\nException: "
                    + e.getMessage() +"\r\n");
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                    // ignore
                }
            }
            if (fc != null) {
                try {
                    fc.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }

    /**
     * delete a file or directory
     */
    void delete (String currFile, OutputWriter writer) {
        try {
            FileConnection fc =
                (FileConnection) Connector.open ("file://localhost" + currDirName + currFile);

            if(fc.isDirectory ()) {
                Enumeration content = fc.list ("*", true);
                //only empty directory can be deleted
                if (!content.hasMoreElements ()) {
                    fc.delete ();
                    writer.line("<<rm,OK,deleted\r\n");
                }
                else {
                    writer.line("<<rm,FAIL,Cannot delete The non-empty folder: " + currFile + "\r\n");
                }
            } else {
                fc.delete ();
                writer.line("<<rm,OK,deleted\r\n");
            }
            fc.close();
        } catch (IOException ioe) {
            writer.line("<<rm,FAIL,Cannot delete: " + currFile + "\r\n");
            ioe.printStackTrace ();
        }
    }

    /**
     * show file or dir's properties
     */
    void showProperties (String fileName, OutputWriter writer) {
        try {
            FileConnection fc =
                (FileConnection) Connector.open ("file://localhost" + currDirName + fileName);

            if (!fc.exists ()) {
                throw new IOException ("File does not exists");
            }

            writer.line("<<prop,Location:" + currDirName);
            writer.line("<<prop,Type: " + (fc.isDirectory () ? "Directory" : "Regular File"));
            writer.line("<<prop,Modified:"+ myDate (fc.lastModified ()));
            writer.line("<<prop,Attributes:"+" Read="+(fc.canRead () ? "yes" : "no")+" Write="+(fc.canWrite () ? "yes" : "no")+" Hidden="+(fc.isHidden () ? "yes" : "no"));

            fc.close ();

            writer.line("<<prop,OK\r\n");
        }
        catch (Exception e) {
            writer.line("<<prop,FAIL,Cannot access " + fileName + " in directory " + currDirName +
                        "\nException: " + e.getMessage ()+"\r\n");
        }
    }

    /**
     * create a file or directory
     */
    void createFile (String newName, boolean isDirectory, OutputWriter writer) {
        try {
            FileConnection fc = (FileConnection) Connector.open ("file://localhost" + currDirName + newName);

            if (isDirectory) {
                fc.mkdir ();
                writer.line("<<new,OK,directory "+ newName +" is created\r\n");
            }
            else {
                fc.create ();
                writer.line("<<new,OK,file "+ newName +" is created\r\n");
            }
        }
        catch (Exception e) {
            String s = "Cannot create file '" + newName + "'";
            if ((e.getMessage () != null) && (e.getMessage ().length () > 0)) {
                s += ("\r\n" + e);
            }
            writer.line("<<new,FAIL,"+ s +"\r\n");
        }
    }

    private String myDate (long time) {
        Calendar cal = Calendar.getInstance ();

        cal.setTime (new Date (time));

        StringBuffer sb = new StringBuffer ();

        sb.append (cal.get (Calendar.HOUR_OF_DAY));
        sb.append (':');
        sb.append (cal.get (Calendar.MINUTE));
        sb.append (':');
        sb.append (cal.get (Calendar.SECOND));
        sb.append (',');
        sb.append (' ');
        sb.append (cal.get (Calendar.DAY_OF_MONTH));
        sb.append (' ');
        sb.append (monthList[cal.get (Calendar.MONTH)]);
        sb.append (' ');
        sb.append (cal.get (Calendar.YEAR));

        return sb.toString ();
    }

    public void rootChanged (int state, String rootName) {
        System.out.println("rootChanged:"+rootName);
    }
    
    /**
     * List command handler
     */
    private class ListCmd implements Command {
        public void execute(String[] param, OutputWriter writer) {
            if(1 != param.length) {
                writer.line("<<ls,FAIL,invalid parameters.\r\n");
            } else {
                showCurrDirNow(writer);
            }
        }

        public String getCommandName() {
            return "ls";
        }

        public String getHelpMessage() {
            return    "<<ls,Usage: ls\r\n"
                    + "<<ls,list information about the FILEs for the current directory\r\n";
        }
    }

    /**
     * Change Dir command handler
     */
    private class ChangeDir implements Command {
        public void execute(String[] param, OutputWriter writer) {
            if(2 != param.length) {
                writer.line("<<cd,FAIL,invalid parameter\r\n");
            } else {
                traverseDirectory(param[1], writer);
            }
        }

        public String getCommandName() {
            return "cd";
        }

        public String getHelpMessage() {
            return    "<<cd,Usage: cd <DIR>\r\n"
                    + "<<cd,change directory\r\n";
        }
    }

    /**
     * Show file command handler
     */
    private class ShowFile implements Command {
        public void execute(String[] param, OutputWriter writer) {
            if(2 != param.length) {
                writer.line("<<view,FAIL,invalid parameters.\r\n");
            } else {
                showFile(param[1], writer);
            }
        }

        public String getCommandName() {
            return "view";
        }

        public String getHelpMessage() {
            return    "<<view,Usage: view <FILE>\r\n"
                    + "<<view,view a file's content\r\n";
        }
    }

    /**
     * Property command handler
     */
    private class Property implements Command {
        public void execute(String[] param, OutputWriter writer) {
            if(2 != param.length) {
                writer.line("<<prop,FAIL,invalid parameters.\r\n");
            } else {
                showProperties(param[1], writer);
            }
        }

        public String getCommandName() {
            return "prop";
        }

        public String getHelpMessage() {
            return    "<<prop,Usage: prop <FILE>\r\n"
                    + "<<prop,show properties of a file\r\n";
        }
    }

    /**
     * New command handler
     */
    private class NewCmd implements Command {
        public void execute(String[] param, OutputWriter writer) {
            if(3 != param.length) {
                writer.line("<<new,FAIL,invalid parameters.\r\n");
            } else {
                if(param[1].equals("file")) {
                    createFile(param[2], false, writer);
                } else if(param[1].equals("dir")) {
                    createFile(param[2], true, writer);
                } else {
                    writer.line("<<new,FAIL,invalid parameters.\r\n");
                }
                
            }
        }

        public String getCommandName() {
            return "new";
        }

        public String getHelpMessage() {
            return    "<<new,Usage: new <file|dir> <NAME>\r\n"
                    + "<<new,create new file or directory\r\n";
        }
    }

    /**
     * Delete command handler
     */
    private class DeleteCmd implements Command {
        public void execute(String[] param, OutputWriter writer) {
            if(2 != param.length) {
                writer.line("<<rm,FAIL,invalid parameters.\r\n");
            } else {
                delete(param[1], writer);
            }
        }

        public String getCommandName() {
            return "rm";
        }

        public String getHelpMessage() {
            return    "<<rm,Usage: rm <NAME>\r\n"
                    + "<<rm,remove the file\r\n";
        }
    }
    
}
