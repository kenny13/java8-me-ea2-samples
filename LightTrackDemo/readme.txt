#
# Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
#

Light Track Sample
------------------

Overview:
---------

This sample demonstrates the following functionality:
- DAAPI open peripheral by config
- DAAPI GPIO port (LEDs)
- DAAPI ADC

The sample flashes on/off corresponded number of available LEDs depending on a current value of ADC Channel.
Best suited for Keil F200 board where one of ADC Channels connected to on-board Potentiometer.

Sample Setup:
-------------

- Install LightTrackDemo.jad/jar

- Start LightTrackIMlet

Behavior Description:
---------------------

- After start the sample reads hardware port/pin numbers from the "LEDsPins" property.
  The property must be in the following string format:
    {portValue1,pinValue1} {portValue2,pinValue2} ... {portValueN,pinValueN}.
  A virtual GPIO port is created from that hardware port/pin numbers.
  
- ADC Channel with id=110 is opened.

- If the specified "LEDsPins" property has wrong format or creation/opening of the virtual GPIO port or the ADC Channel
  is failed then the sample writes a message to the system log and terminates.

- The sample get min and max values of the ADC Channel and divides that range to (number_of_leds + 1) intervals.

- After that the sample monitors the current value of the ADC Channel
  and flashes on/off corresponded number of the specified LEDs.
  If the current value is:
  -- in the first interval: all LEDs are OFF;
  -- in the second interval: the first LED specified in the "LEDsPins" property is ON, all other are OFF;
  -- in the third interval: the first and the second LEDs specified in the "LEDsPins" property are ON, all other are OFF;
  -- ...
  -- in the last interval: all LEDs are ON.

- To change the ADC Channel value -
  on Keil F200: use on-board Potentiometer;
  on ME SDK: use emulated ADC Channel with id=110, name=ADC1Ch1
