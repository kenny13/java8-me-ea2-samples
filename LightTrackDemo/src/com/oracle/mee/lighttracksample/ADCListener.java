/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.lighttracksample;

import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.adc.ADCChannel;
import com.oracle.deviceaccess.adc.MonitoringEvent;
import com.oracle.deviceaccess.adc.MonitoringListener;
import java.io.IOException;

/**
 *Controls ADC channel. Monitors changing of ADC channel value.
 */
public class ADCListener implements MonitoringListener {

    private final static int ADC_ID = 110;
    private ADCChannel channel;
    private Interval[] intervals;
    private LEDPort port;
    private int count;
    private int intervalsNum;
    private int interval;

    ADCListener(LEDPort port) {
        this.port = port;
    }

    public void failed(Throwable t, ADCChannel adcChannel) {
    }
    
    /**
     * Starts monitoring ADC channel.
     */
    public boolean start() {
        boolean isStarted = false;
        count = port.getLEDsCount();
        intervalsNum = count + 1;
        try {
            channel = (ADCChannel) PeripheralManager.open(ADC_ID);
            createIntervals();
            interval = findInterval(channel.acquire());
            channel.startMonitoring(intervals[interval].getLowBorder(),
                    intervals[interval].getHighBorder(), this);
            port.setValue(intervals[interval].getPortValue());
            isStarted = true;
        } catch (PeripheralNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
        return isStarted;
    }
    
    /**
     * Stops monitoring ADC channel.
     */
    public void stop() {
        if (channel != null) {
            try {
                channel.stopMonitoring();
                channel.close();
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        }

    }
    
    /**
     * Fills up <code>Intervals<code> list.
     * Actual number of the intervals is equal to number of LEDs plus one.
     *
     * @see Interval
     */
    private void createIntervals() {
        int portValue;
        intervals = new Interval[intervalsNum];
        try {
            int maxValue = channel.getMaxValue();
            int minValue = channel.getMinValue();
	        int intervalLenght = ((maxValue - minValue) + 1) / intervalsNum;
	        int intervalMask = (1 << count) - 1;
	
	        maxValue = minValue;
	        for (int i = 0; i < intervalsNum; i++) {
	            minValue = maxValue;
	            intervals[i] = new Interval();
	            intervals[i].setLowBorder(minValue);
	            maxValue = (i != intervalsNum - 1)
	                    ? minValue + intervalLenght : channel.getMaxValue() + 1;
	            intervals[i].setHighBorder(maxValue - 1);
	            //portValue calculates like (2^i) - 1
	            portValue = intervalMask >>> (count - i);
	            intervals[i].setPortValue(portValue);
	        }
        } catch (Throwable ex) {
        	ex.printStackTrace();
        }
    }
    
    /**
     * Finds an interval that is corresponed to the provided ADC value.
     * @param value current ADC value
     * @return interval number
     */
    private int findInterval(int value) {
        for (int i = 0; i < intervalsNum; i++) {
            if (value >= intervals[i].getLowBorder()
                    & value <= intervals[i].getHighBorder()) {
                return i;
            }
        }
        return 0;
    }
    
    public void thresholdReached(MonitoringEvent me) {
        int new_interval = findInterval(me.getValue());
        try {
            if (new_interval != interval) {
                channel.stopMonitoring();
                //starts new monitoring with new low and high values
                channel.startMonitoring(intervals[new_interval].getLowBorder(),
                        intervals[new_interval].getHighBorder(), this);
                port.setValue(intervals[new_interval].getPortValue());
                interval = new_interval;
            }
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Interval object is characterized by two main parameters. 
     * The first one is an ADC channel values range 
     * [<code>lowBorder</code>, <code>highBorder</code>]. 
     * The second one is a <code>portValue</code> that is set to a virtual 
     * <code>GPIOPort</code> if a current ADC channel value fills in 
     * [<code>lowBorder</code>, <code>highBorder</code>]. 
     * <code>portValue</code> is a mask of all available LEDs. 
     * Each bit of a mask either turn on or turn off a particular LED.
     */
    private class Interval {

        private int lowBorder, highBorder, portValue;

        public int getLowBorder() {
            return lowBorder;
        }

        public int getHighBorder() {
            return highBorder;
        }

        public int getPortValue() {
            return portValue;
        }

        public void setLowBorder(int value) {
            lowBorder = value;
        }

        public void setHighBorder(int value) {
            highBorder = value;
        }

        public void setPortValue(int value) {
            portValue = value;
        }
    }
}
