/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.lighttracksample;

import com.oracle.deviceaccess.PeripheralConfigInvalidException;
import com.oracle.deviceaccess.PeripheralExistsException;
import com.oracle.deviceaccess.PeripheralManager;
import com.oracle.deviceaccess.UnavailablePeripheralException;
import com.oracle.deviceaccess.PeripheralNotFoundException;
import com.oracle.deviceaccess.PeripheralTypeNotSupportedException;
import com.oracle.deviceaccess.gpio.GPIOPinConfig;
import com.oracle.deviceaccess.gpio.GPIOPort;
import com.oracle.deviceaccess.gpio.GPIOPortConfig;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Represents a virtual LED port.
 */
public class LEDPort {
    
    private int count;
    private GPIOPinConfig pins[];
    private GPIOPort port;
    
    final static int PARSER_START_STATE      = 0;
    final static int PARSER_PORT_START_STATE = 1;
    final static int PARSER_PIN_START_STATE  = 2;
    final static int PARSER_END_STATE        = 3;
    
    final static char PARSER_OPENING_BRACE  = '{';
    final static char PARSER_CLOSING_BRACE  = '}';
    final static char PARSER_COMMA          = ',';
    
    final static String INVALID_FORMAT = "Invalid ports/pins format string:";

    /**
     * Initializes a virtual port that is mapped onto the set of configured 
     * GPIO pins.
     */
    public boolean initialize() {
        boolean isPortOpened = false;
        Vector leds;
        try {
            leds = getLEDsPins(LightTrackIMlet.getLEDsPins());
        } catch (Exception ex) {
            System.out.println("LEDPort : " + ex.getMessage());
            return false;
        }

        count = leds.size();
        pins = new GPIOPinConfig[count];

        Enumeration ledsEnum = leds.elements();
        while (ledsEnum.hasMoreElements()) {
            LED led = (LED) ledsEnum.nextElement();
            pins[leds.indexOf(led)] = new GPIOPinConfig(
                    led.getPort(), led.getPin(),
                    GPIOPinConfig.DIR_OUTPUT_ONLY,
                    GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                    GPIOPinConfig.TRIGGER_BOTH_EDGES,
                    false);
        }
        try {
            port = (GPIOPort) PeripheralManager.open(new GPIOPortConfig(GPIOPortConfig.DIR_OUTPUT_ONLY, 0, pins));
            isPortOpened = true;
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (PeripheralConfigInvalidException ex) {
            ex.printStackTrace();
        } catch (PeripheralTypeNotSupportedException ex) {
            ex.printStackTrace();
        } catch (PeripheralNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (PeripheralExistsException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return isPortOpened;
    }

    /**
     * Closes opened virtual port.
     */
    public void uninitialize() {
        try {
            if (port != null) {
                port.setValue(0);
                port.close();
            }
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Sets the <code>value</code> of opened virtual port.
     * @param value settable value
     */
    public void setValue(int value) {
        try {
            port.setValue(value);
        } catch (UnavailablePeripheralException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Returns number of pin/port pairs defined in JAD property.
     * @return LEDs count
     */
    public int getLEDsCount() {
        return count;
    }

    /**
     * Parses string of hardware LEDs pins and ports in the following format: 
     * {portValue1,pinValue1} ... {portValueN,pinValueN}
     * @param str JAD property string of pins and ports numbers
     * @return vector of LED objects
     */
    private Vector getLEDsPins(String str) throws Exception {
        Vector leds = new Vector();
        LED led = null;
        char data[] = str.toCharArray();
        int state = PARSER_START_STATE;
        int dataStartIdx = 0, dataEndIdx = 0;

        for (int i = 0; i < data.length; i++) {
            switch (state) {
                case PARSER_START_STATE:
                    if (data[i] != PARSER_OPENING_BRACE) {
                        throw new Exception(INVALID_FORMAT + str);
                    }
                    state = PARSER_PORT_START_STATE;
                    dataStartIdx = i + 1;
                    dataEndIdx = dataStartIdx;
                    break;

                case PARSER_PORT_START_STATE:
                case PARSER_PIN_START_STATE:
                    if (Character.isDigit(data[i])) {
                        dataEndIdx = i + 1;
                        continue;

                    } else if (data[i] == PARSER_COMMA && state == PARSER_PORT_START_STATE && dataStartIdx != dataEndIdx) {
                        state = PARSER_PIN_START_STATE;
                        led = new LED();
                        led.setPort(Integer.parseInt(str.substring(dataStartIdx, dataEndIdx)));

                    } else if (data[i] == PARSER_CLOSING_BRACE && state == PARSER_PIN_START_STATE && dataStartIdx != dataEndIdx) {
                        state = PARSER_END_STATE;
                        led.setPin(Integer.parseInt(str.substring(dataStartIdx, dataEndIdx)));
                        leds.addElement(led);

                    } else {
                        throw new Exception(INVALID_FORMAT + str);
                    }

                    //If the state machine is here then either port or pin is extracted from the config
                    dataStartIdx = i + 1;
                    dataEndIdx = dataStartIdx;
                    break;

                case PARSER_END_STATE:
                    if (data[i] != ' ') {
                        throw new Exception(INVALID_FORMAT + str);
                    }
                    state = PARSER_START_STATE;
                    break;
                default:
            }
        }
        return leds;
    }
    
    /**Pin and port number for one LED.
     */
    public class LED{

        private int pin, port;

        public int getPin() {
            return pin;
        }

        public int getPort() {
            return port;
        }

        public void setPin(int value) {
            pin = value;
        }

        public void setPort(int value) {
            port = value;
        }
    }
}
