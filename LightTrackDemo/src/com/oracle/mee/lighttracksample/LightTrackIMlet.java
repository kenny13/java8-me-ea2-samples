/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 */
package com.oracle.mee.lighttracksample;

import javax.microedition.midlet.*;

/**
 * Lights on/off the sequence of LEDs depending on a value of ADC Channel.
 * 
 * <p><b>Target Platforms</b>: emulator and KEIL MCBSTM32F200 development board.</p>
 * <p>For more information, please see the accompanying readme.txt file</p>
 *
 * @see LEDPort
 * @see ADCListener
 */
public class LightTrackIMlet extends MIDlet {
    
    private LEDPort port;
    private ADCListener adcListener;
    private static String LEDsPins;
    
    /**
     * Returns JAD property string LEDsPins with LEDs hardware port-pins numbers.
     */
    public static String getLEDsPins() {
        return LEDsPins;
    }

    public void startApp() {
        
        LEDsPins = getAppProperty("LEDsPins");
        
        System.out.println("*********************************");
        System.out.println("*      Light Track Sample       *");
        System.out.println("*********************************");
        
        System.out.println("Initializing LEDs port...");
        port = new LEDPort();

        if (port.initialize()) {
            System.out.println("LEDs port initialization is OK");
            System.out.println("Starting ADCListener...");
            adcListener = new ADCListener(port);
            if (adcListener.start()) {
                System.out.println("ADCListener start is OK");
            }else {
                System.out.println("ADCListener start is failed.");
                notifyDestroyed();
            }
        } else {
            System.out.println("LEDs port initialization is failed.");
            notifyDestroyed();
        }
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
        if (adcListener != null) {
            adcListener.stop();
        }
        if (port != null) {
            port.uninitialize();
        }
    }

}
